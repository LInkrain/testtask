﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Figures.Interfaces;
using Figures;

namespace UnitTestFigures
{
    [TestClass]
    public class UnitTestFiguresParams
    {
        [TestMethod]
        public void TestCirlceSquare()
        {
            double radius = 10;
            IFigure circle = new Circle(radius);
            double circleSquare = Math.PI * radius* radius;
            Assert.AreEqual(circleSquare, circle.Square);
        }

        [TestMethod]
        public void TestTriangleSquare()
        {
            double a = 5, b = 3, c=7;
            IFigure triangle = new Triangle(a,b,c);
            double p = (a + b + c) / 2;
            double triangleSquare = Math.Sqrt(p * (p - 2) * (p - 2) * (p - c));
            Assert.AreEqual(triangleSquare, triangle.Square);
        }

        [TestMethod]
        public void TestISRectangle()
        {
            double a = 3, b = 4, c = 5;
            Triangle triangle = new Triangle(a, b, c);           
            Assert.AreEqual(true, triangle.IsRectangle);
        }
    }
}
