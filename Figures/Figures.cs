﻿using System;
using Figures.Interfaces;

namespace Figures
{
   public class Circle : IFigure
    {
        double radius;

        public Circle(double radius)
        {
            this.radius = radius;
        }

        public double Radius
        {
            get
            {
                return radius;
            }
            set
            {
                radius = value;
            }
        }

        public double Square => radius * radius * Math.PI;
        public double Perimeter => 2 * Math.PI * radius;
    }

    public class Triangle : IFigure
    {
        double a;
        double b;
        double c;

        public Triangle(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public double A
        {
            get
            {
                return a;
            }
            set
            {
                a = value;
            }
        }

        public double B
        {
            get
            {
                return b;
            }
            set
            {
                b = value;
            }
        }

        public double C
        {
            get
            {
                return c;
            }
            set
            {
                c = value;
            }
        }
     
        public double Square
        {
            get
            {
                if (IsRectangle)
                {
                    return (a * b) / 2;
                }
                else
                {
                    double p = Perimeter;
                    return Math.Sqrt(p * (p - 2) * (p - 2) * (p - c));
                }
            }
        }

        public bool IsRectangle => (a * a + b * b) == (c * c);        
        public double Perimeter => (a + b + c) / 2;
    }
}
