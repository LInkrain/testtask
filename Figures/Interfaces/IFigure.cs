﻿namespace Figures.Interfaces
{
    public interface IFigure
    {
        double Square { get; }
        double Perimeter { get; }
    }
}
